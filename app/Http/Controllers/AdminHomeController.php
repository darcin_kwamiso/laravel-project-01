<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Project;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with("projects")->get();
        $users = User::where('role', '!=', 'admin')->orderBy('created_at', 'desc')->get();
        $projects = Project::orderBy('created_at', 'desc')->get();

        $new_users = User::where('role', '!=', 'admin')->whereDate('created_at', Carbon::today())->get();
        $new_projects = Project::whereDate('created_at', Carbon::today())->get();

        $jan = Project::whereMonth('created_at', '01')->get()->count();
        $feb = Project::whereMonth('created_at', '02')->get()->count();
        $mar = Project::whereMonth('created_at', '03')->get()->count();
        $apr = Project::whereMonth('created_at', '04')->get()->count();
        $may = Project::whereMonth('created_at', '05')->get()->count();
        $jun = Project::whereMonth('created_at', '06')->get()->count();
        $jul = Project::whereMonth('created_at', '07')->get()->count();
        $aug = Project::whereMonth('created_at', '08')->get()->count();
        $sep = Project::whereMonth('created_at', '09')->get()->count();
        $oct = Project::whereMonth('created_at', '10')->get()->count();
        $nov = Project::whereMonth('created_at', '11')->get()->count();
        $dec = Project::whereMonth('created_at', '12')->get()->count();

        $months = array(
            (object) ['name' => 'January', 'projects' => $jan],
            (object) ['name' => 'February', 'projects' => $feb],
            (object) ['name' => 'March', 'projects' => $mar],
            (object) ['name' => 'April', 'projects' => $apr],
            (object) ['name' => 'May', 'projects' => $may],
            (object) ['name' => 'June', 'projects' => $jun],
            (object) ['name' => 'July', 'projects' => $jul],
            (object) ['name' => 'August', 'projects' => $aug],
            (object) ['name' => 'September', 'projects' => $sep],
            (object) ['name' => 'October', 'projects' => $oct],
            (object) ['name' => 'November', 'projects' => $nov],
            (object) ['name' => 'December', 'projects' => $dec],
        );

        return view('admin.dashboard')->with(['categories' => $categories, 'users' => $users, 'projects' => $projects, 'new_users' => $new_users, 'new_projects' => $new_projects, 'months' => $months]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Project;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with("projects")->get();
        $users = User::where('role', '!=', 'admin')->orderBy('created_at', 'desc')->get();
        $my_projects = DB::table('categories')
            ->join('projects', 'categories.id', '=', 'projects.category_id')
            ->where('user_id', '=', Auth::id())
            ->orderBy('projects.created_at', 'desc')
            ->paginate(5);

        $jan = Project::whereMonth('created_at', '01')->where('user_id', '=', Auth::id())->get()->count();
        $feb = Project::whereMonth('created_at', '02')->where('user_id', '=', Auth::id())->get()->count();
        $mar = Project::whereMonth('created_at', '03')->where('user_id', '=', Auth::id())->get()->count();
        $apr = Project::whereMonth('created_at', '04')->where('user_id', '=', Auth::id())->get()->count();
        $may = Project::whereMonth('created_at', '05')->where('user_id', '=', Auth::id())->get()->count();
        $jun = Project::whereMonth('created_at', '06')->where('user_id', '=', Auth::id())->get()->count();
        $jul = Project::whereMonth('created_at', '07')->where('user_id', '=', Auth::id())->get()->count();
        $aug = Project::whereMonth('created_at', '08')->where('user_id', '=', Auth::id())->get()->count();
        $sep = Project::whereMonth('created_at', '09')->where('user_id', '=', Auth::id())->get()->count();
        $oct = Project::whereMonth('created_at', '10')->where('user_id', '=', Auth::id())->get()->count();
        $nov = Project::whereMonth('created_at', '11')->where('user_id', '=', Auth::id())->get()->count();
        $dec = Project::whereMonth('created_at', '12')->where('user_id', '=', Auth::id())->get()->count();

        $months = array(
            (object) ['name' => 'January', 'projects' => $jan],
            (object) ['name' => 'February', 'projects' => $feb],
            (object) ['name' => 'March', 'projects' => $mar],
            (object) ['name' => 'April', 'projects' => $apr],
            (object) ['name' => 'May', 'projects' => $may],
            (object) ['name' => 'June', 'projects' => $jun],
            (object) ['name' => 'July', 'projects' => $jul],
            (object) ['name' => 'August', 'projects' => $aug],
            (object) ['name' => 'September', 'projects' => $sep],
            (object) ['name' => 'October', 'projects' => $oct],
            (object) ['name' => 'November', 'projects' => $nov],
            (object) ['name' => 'December', 'projects' => $dec],
        );

        return view('user.dashboard', ['categories' => $categories, 'users' => $users, 'my_projects' => $my_projects, 'months' => $months]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

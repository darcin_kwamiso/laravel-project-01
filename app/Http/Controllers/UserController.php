<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_users = User::where('id', '!=', Auth::id())->orderBy('updated_at', 'desc')->paginate(5);

        $all_projects = Project::all();

        return view('admin.users', [
            'all_users' => $all_users,
            'all_projects' => $all_projects,
            'title' => 'All Users',
            'desc' => 'This is meta description for all Users',
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'gender' => ['required', 'string', 'max:255'],
            'title' => ['required', 'string', 'max:255'],
            'role' => ['required', 'string', 'max:255'],
            'password' => ['required', 'confirmed'],
        ]);

        if ($request->hasFile('avatar')) {
            $avatar = Storage::disk('local')->put('avatars', $request->file('avatar'));
            $user = User::create([
                'avatar' => $avatar,
                'name' => $request->name,
                'email' => $request->email,
                'gender' => $request->gender,
                'phone' => $request->phone,
                'title' => $request->title,
                'role' => $request->role,
                'password' => Hash::make($request->password),
            ]);
        } else {
            $user = User::create([
                'avatar' => 'N/A',
                'name' => $request->name,
                'email' => $request->email,
                'gender' => $request->gender,
                'phone' => $request->phone,
                'title' => $request->title,
                'role' => $request->role,
                'password' => Hash::make($request->password),
            ]);
        }

        event(new Registered($user));

        Auth::login($user);

        return redirect('register')->with('success', 'user successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $my_profile = User::findOrFail($id);

        $my_projects = DB::table('categories')
            ->join('projects', 'categories.id', '=', 'projects.category_id')
            ->where('user_id', '=', Auth::id())
            ->orderBy('projects.created_at', 'desc')
            ->get();

        $assigned_projects = DB::table('assign_users')
            ->join('projects', 'projects.id', '=', 'assign_users.project_id')
            ->where('assign_users.user_id', '=', Auth::id())
            ->orderBy('projects.created_at', 'desc')
            ->select('projects.*')
            ->get();

        return view('profile', [
            'my_profile' => $my_profile,
            'my_projects' => $my_projects,
            'assigned_projects' => $assigned_projects,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_user = User::findOrFail($id);

        return view('admin.user_form', [
            'edit_user' => $edit_user,
            'title' => ucfirst($edit_user->name),
            'desc' => 'This is meta description for User Edit',
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'gender' => ['string', 'max:1'],
            'title' => ['required', 'string', 'max:255'],
            'phone' => ['string', 'max:255'],
            'current_pass' => ['string', 'max:255'],
        ]);

        if ($request->hasFile('avatar')) {
            $avatar = Storage::disk('public')->put('avatars', $request->file('avatar'));
            $user = User::findOrFail($id)->update([
                'avatar' => $avatar,
                'name' => $request->fname . ' ' . $request->lname,
                'email' => $request->email,
                'gender' => $request->gender,
                'phone' => $request->phone,
                'title' => $request->title,
                'password' => $request->filled('new_pass') && $request->filled('confirm_pass') && $request->new_pass == $request->confirm_pass ? Hash::make($request->new_pass) : $request->current_pass,
            ]);
        } else {
            $user = User::findOrFail($id)->update([
                'name' => $request->fname . ' ' . $request->lname,
                'email' => $request->email,
                'gender' => $request->gender,
                'phone' => $request->phone,
                'title' => $request->title,
                'password' => $request->filled('new_pass') && $request->filled('confirm_pass') && $request->new_pass == $request->confirm_pass ? Hash::make($request->new_pass) : $request->current_pass,
            ]);
        }

        dd($request->hasFile('avatar'));

        // event(new Registered($user));

        // Auth::login($user);

        if (Auth::user()->role == 'admin') {
            return redirect('users/' . Auth::id())->with('success', 'user successfully updated!');
        } else {
            return redirect('user/' . Auth::id())->with('success', 'user successfully updated!');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        redirect('admin/users')->with('success', 'user successfully created!');
    }
}

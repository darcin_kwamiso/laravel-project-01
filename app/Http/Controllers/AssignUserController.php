<?php

namespace App\Http\Controllers;

use App\Models\AssignUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AssignUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'roles' => 'string|required',
        ]);

        $user_exist = AssignUser::where(['project_id' => $request->project_id, 'user_id' => $request->user_id])->exists();

        $assignedViewer = AssignUser::where(['project_id' => $request->project_id, 'roles' => 'viewer'])->count();
        $assignedAuthor = AssignUser::where(['project_id' => $request->project_id, 'roles' => 'author'])->count();

        // dd($request->roles);

        if ($assignedAuthor < 2 && $request->roles == 'author' && $user_exist == false) {
            AssignUser::create([
                'project_id' => $request->project_id,
                'user_id' => $request->user_id,
                'roles' => $request->roles,
            ]);
            return redirect('/my/project/' . $request->project_id)->with('success', 'successfully assigned as ' . $request->roles . '!');
        } else if ($assignedViewer < 5 && $request->roles == 'viewer' && $user_exist == false) {
            AssignUser::create([
                'project_id' => $request->project_id,
                'user_id' => $request->user_id,
                'roles' => $request->roles,
            ]);
            return redirect('/my/project/' . $request->project_id)->with('success', 'successfully assigned as ' . $request->roles . '!');
        } else {
            return redirect('/my/project/' . $request->project_id)->with('error', "you've reached the number users to assign to this project");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AssignUser::where('user_id', '=', $id)->delete();

        return Redirect::back()->with('success', 'successfully removed!');
    }
}

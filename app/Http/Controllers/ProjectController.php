<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Document;
use App\Models\Image;
use App\Models\Project;
use App\Models\User;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_users = User::orderBy('updated_at', 'desc')->get();

        $all_projects = Project::orderBy('updated_at', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate(5);

        $my_projects = DB::table('categories')
            ->join('projects', 'categories.id', '=', 'projects.category_id')
            ->where('user_id', '=', Auth::id())
            ->orderBy('projects.created_at', 'desc')
            ->paginate(5);

        $assigned_projects = DB::table('assign_users')
            ->join('projects', 'projects.id', '=', 'assign_users.project_id')
            ->where('assign_users.user_id', '=', Auth::id())
            ->orderBy('projects.created_at', 'desc')
            ->select('projects.*')
            ->paginate(7);

        if (Auth::user()->role == 'admin') {
            return view('admin.projects', [
                'all_projects' => $all_projects,
                'all_users' => $all_users,
                'title' => 'Projects',
                'desc' => 'This is meta description for all Projects',
            ]);
        } else {
            return view('user.projects', [
                'my_projects' => $my_projects,
                'assigned_projects' => $assigned_projects,
                'all_users' => $all_users,
                'title' => 'Projects',
                'desc' => 'This is meta description for all Projects',
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $p_image = Image::where('user_id', Auth::id())->orderBy('created_at', 'desc')->first();
        $categories = Category::all();

        $my_projects = DB::table('categories')
            ->join('projects', 'categories.id', '=', 'projects.category_id')
            ->where('user_id', '=', Auth::id())
            ->orderBy('projects.created_at', 'desc')
            ->paginate(4);

        return view('user.project_form', ['my_projects' => $my_projects, 'categories' => $categories, 'p_image' => $p_image]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->file('video_upload'));

        $request->validate([
            'name' => 'string|required|unique:projects|max:500',
            'description' => 'string|required|min:10|max:5000|unique:projects',
            'link' => 'string|min:10|required',
            'status' => 'required|string',
        ]);

        $project = Project::create([
            'name' => ucfirst($request->name),
            'category_id' => $request->category,
            'description' => ucfirst($request->description),
            'link' => strtolower($request->link),
            'image_url' => $request->hasFile('image_upload') ? Storage::disk('public')->put('images', $request->file('image_upload')) : 'N/A',
            'user_id' => Auth::id(),
            'status' => ucfirst($request->status),
        ]);

        if ($request->hasFile('image_upload')) {
            $image = Storage::disk('my_files')->put('images', $request->file('image_upload'));
            Image::create(['image_url' => $image, 'user_id' => Auth::id(), 'project_id' => $project->id]);
        } elseif ($request->hasFile('video_upload')) {
            $video = Storage::disk('public')->put('videos', $request->file('video_upload'));
            Video::create(['video_url' => $video, 'user_id' => Auth::id(), 'project_id' => $project->id]);
        } elseif ($request->hasFile('document_upload')) {
            $document = Storage::disk('public')->put('documents', $request->file('document_upload'));
            Document::create(['document_url' => $document, 'user_id' => Auth::id(), 'project_id' => $project->id]);
        }

        return redirect('my/project/create')->with('success', 'successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $current_project = DB::table('categories')
            ->join('projects', 'categories.id', '=', 'projects.category_id')
            ->where('user_id', '=', Auth::id())
            ->where('projects.id', '=', $id)
            ->first();

        $users = User::where('id', '!=', Auth::id())->where('role', '!=', 'admin')->orderBy('updated_at', 'desc')->get();
        $assigned_users = DB::table('assign_users')->join('users', 'assign_users.user_id', '=', 'users.id')->where('project_id', '=', $id)->select('users.*')->get();

        // dd($assigned_users);

        $all_users = [];

        // if ($assigned_users->count() > 0) {
        //     foreach ($assigned_users as $assigned) {
        //         foreach ($users as $user) {
        //             if ($assigned->id != $user->id) {
        //                 if (in_array($user, $all_users)) {
        //                     $all_users = $all_users;
        //                 } else {
        //                     array_push($all_users, $user);
        //                 }
        //             }
        //         }
        //     }
        // } else {
        //     $all_users = $users;
        // }
        if ($assigned_users->count() > 0) {
            foreach ($users as $user) {
                foreach ($assigned_users as $assigned) {
                    if (in_array($user, $all_users)) {
                        $all_users = $all_users;
                    } else {
                        array_push($all_users, $user);
                    }
                }
            }
        } else {
            $all_users = $users;
        }

        // dd($all_users);

        // $all_users = DB::table('assign_users')->join('users', 'assign_users.user_id', '!=', 'users.id')->where('users.role', '!=', 'admin')->where('users.id', '!=', Auth::id())->select('users.*')->get();

        return view('user.project_details', [
            'all_users' => $all_users,
            'assigned_users' => $assigned_users,
            'current_project' => $current_project,
            'title' => ucfirst($current_project->name),
            'desc' => 'This is meta description for Project Details',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_project = Project::findOrFail($id);

        $my_projects = DB::table('categories')
            ->join('projects', 'categories.id', '=', 'projects.category_id')
            ->where('user_id', '=', Auth::id())
            ->orderBy('projects.created_at', 'desc')
            ->paginate(4);

        return view('user.project_form', [
            'edit_project' => $edit_project,
            'my_projects' => $my_projects,
            'title' => ucfirst($edit_project->name),
            'desc' => 'This is meta description for Project Edit',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'string|required|max:500',
            'description' => 'string|required|min:10|max:5000',
            'link' => 'string|min:10|required',
            'status' => 'string|required',
        ]);

        if ($request->hasFile('image_upload') == true) {
            $image = Storage::disk('public')->put('images', $request->file('image_upload'));

            Project::findOrFail($id)->update([
                'name' => ucfirst($request->name),
                'image_url' => $image,
                'description' => ucfirst($request->description),
                'link' => strtolower($request->link),
                'user_id' => Auth::id(),
                'status' => ucfirst($request->status),
            ]);
        } else {
            Project::findOrFail($id)->update([
                'name' => ucfirst($request->name),
                'description' => ucfirst($request->description),
                'link' => $request->link,
                'user_id' => Auth::id(),
                'status' => ucfirst($request->status),
            ]);
        }

        return redirect('my/project/' . $id . '/edit')->with('success', 'successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::findOrFail($id)->delete();

        if (Auth::user()->role == 'admin') {
            return redirect('admin/projects')->with('success', 'successfully deleted!');
        } else {
            return redirect('my/project')->with('success', 'successfully deleted!');
        }
    }
}

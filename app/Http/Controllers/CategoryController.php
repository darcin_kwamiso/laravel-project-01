<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_categories = Category::orderBy('updated_at', 'desc')->get();

        return view('admin.category_form', [
            'all_categories' => $all_categories,
            'title' => 'Projects',
            'desc' => 'This is meta description for all categories',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'string|required|unique:categories|max:150',
            'desc' => 'string|required|min:10|max:500|unique:categories',
        ]);

        Category::create([
            'title' => $request->title,
            'desc' => $request->desc,
        ]);

        return redirect('admin/categories')->with('success', 'successfully created!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.category_form', ['edit_category' => $category]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'string|required|max:150',
            'desc' => 'string|required|min:10|max:500',
        ]);

        Category::findOrFail($id)->update([
            'title' => $request->title,
            'desc' => $request->desc,
        ]);

        return redirect('admin/categories')->with('success', 'successfully updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::findOrFail($id)->delete();

        return redirect('admin/categories')->with('success', 'successfully deleted!');
    }
}

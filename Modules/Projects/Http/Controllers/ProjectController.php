<?php

namespace Modules\Projects\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_projects = Project::orderBy('updated_at', 'desc')->get();

        return view('projects', [
            'all_projects' => $all_projects,
            'title' => 'Projects',
            'desc' => 'This is meta description for all Projects',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('project_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'string|required|unique:projects|max:500',
            'description' => 'string|required|min:10|max:5000|unique:projects',
            'user_id' => 'integer|required',
            'link' => 'string|min:10|required',
        ]);

        $image;
        $video;
        $document;

        if ($request->hasFile('image') && $request->hasFile('document')) {
            $image = Storage::disk('public')->put('images', $request->file('image'));
        } elseif ($request->hasFile('video')) {
            $video = Storage::disk('public')->put('videos', $request->file('video'));
        } elseif ($request->hasFile('document')) {
            $document = Storage::disk('public')->put('documents', $request->file('document'));
        }

        Project::create([
            'name' => ucfirst($request->name),
            'image' => $request->hasFile('image') ? $image : 'N/A',
            'video' => $request->hasFile('video') ? $video : 'N/A',
            'document' => $request->hasFile('document') ? $document : 'N/A',
            'description' => ucfirst($request->description),
            'link' => $request->link,
            'user_id' => $request->user_id,
            'status' => ucfirst($request->status),
        ]);
        return redirect('projects')->with('success', 'successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show_project = Project::findOrFail($id);
        $other_projects = Project::where('id', "!=", $id)->orderBy('updated_at', 'desc')->get();

        return view('projects', [
            'show_project' => $show_project,
            'title' => ucfirst($show_project->name),
            'desc' => 'This is meta description for Project Edit',
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_project = Project::findOrFail($id);

        return view('projects', [
            'edit_project' => $edit_project,
            'title' => ucfirst($edit_project->name),
            'desc' => 'This is meta description for Project Edit',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'string|required|unique:projects|max:500',
            'description' => 'string|required|min:10|max:5000|unique:projects',
            'user_id' => 'integer|required',
            'link' => 'string|min:10|required',
        ]);

        $image;
        $video;
        $document;

        if ($request->hasFile('image') && $request->hasFile('document')) {
            $image = Storage::disk('public')->put('images', $request->file('image'));
        } elseif ($request->hasFile('video')) {
            $video = Storage::disk('public')->put('videos', $request->file('video'));
        } elseif ($request->hasFile('document')) {
            $document = Storage::disk('public')->put('documents', $request->file('document'));
        }

        Project::findOrFail($id)->update([
            'name' => ucfirst($request->name),
            'image' => $request->hasFile('image') ? $image : 'N/A',
            'video' => $request->hasFile('video') ? $video : 'N/A',
            'document' => $request->hasFile('document') ? $document : 'N/A',
            'description' => ucfirst($request->description),
            'link' => $request->link,
            'user_id' => $request->user_id,
            'status' => ucfirst($request->status),
        ]);
        return redirect('projects')->with('success', 'successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::findOrFail($id)->delete();

        return redirect('projects')->with('success', 'successfully deleted!');

    }
}

<x-guest-layout>
    <x-auth-card>
        <!-- <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot> -->

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
            @csrf
            <div>
                <label class="block text-sm font-medium text-gray-700">
                    Photo
                </label>
                <div class="mt-2 flex items-center">
                    <span class="inline-block h-16 w-16 rounded-full overflow-hidden bg-gray-100">
                        <img class="h-full w-full text-gray-300 object-cover" id="preview-image-before-upload" src="" alt="">
                    </span>
                    <label for="file-upload" class="relative cursor-pointer bg-white font-medium focus-within:outline-none">
                        <span class="ml-5 bg-white py-2 px-2 border border-gray-300 rounded-md shadow-sm text-xs leading-4 font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Change</span>
                        <input id="file-upload" name="avatar" type="file" class="sr-only">
                    </label>
                </div>
            </div>
                <!-- Name -->
                <div class="col-span-3">
                    <x-label for="name" :value="__('Name')" />

                    <x-input id="name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" type="text" name="name" :value="old('name')" required autofocus />
                </div>

                <!-- Email Address -->
                <div class="col-span-3">
                    <x-label for="email" :value="__('Email')" />

                    <x-input id="email" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" type="email" name="email" :value="old('email')" required />
                </div>

                <!-- Phone Number -->
                <div class="col-span-3">
                    <x-label for="phone" :value="__('Phone')" />

                    <x-input id="phone" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" type="tel" name="phone" :value="old('phone')" required />
                </div>

                <!-- Title -->
                <div class="col-span-3">
                    <x-label for="title" :value="__('Title')" />

                    <x-input id="title" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" type="text" name="title" :value="old('title')" required />
                </div>
                <!-- Password -->
                <div class="col-span-3">
                    <x-label for="password" :value="__('Password')" />

                    <x-input id="password" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                                    type="password"
                                    name="password"
                                    required autocomplete="new-password" />
                </div>

                <!-- Confirm Password -->
                <div class="col-span-3">
                    <x-label for="password_confirmation" :value="__('Confirm Password')" />

                    <x-input id="password_confirmation" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                        type="password"
                        name="password_confirmation" required />
                </div>

                <div class="flex justify-center mt-3">
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                        {{ __('Already registered?') }}
                    </a>
                </div>


            <x-button class="mt-3 group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
                {{ __('Register') }}
            </x-button>
        </form>
    </x-auth-card>
</x-guest-layout>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script type="text/javascript">

    $(document).ready(function (e) {
        $('#file-upload').change(function(){
            let reader = new FileReader();

            reader.onload = (e) => {
                $('#preview-image-before-upload').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        });
    });

</script>

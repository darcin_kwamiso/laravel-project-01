<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Profile') }}
        </h2>
    </x-slot>

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="border-t-4 border-teal-500 rounded-b px-4 py-3 shadow-md bg-red-500 text-white absolute z-50" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Error</p>
                    <p class="text-sm">{{$error}}</p>
                </div>
            </div>
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
        <div class="border-t-4 border-teal-500 rounded-b px-4 py-3 shadow-md bg-green-500 text-white absolute z-50" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Success</p>
                    <p class="text-sm">{{session('success')}}</p>
                </div>
            </div>
        </div>
    @endif

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <form action="{{route('user.update', Auth::id())}}" method="POST">
                @csrf
                @method('put')

            <div class="grid grid-cols-8 space-x-8">
                <div class="col-span-2">
                    <div class="bg-white p-3 border-t-4 border-blue-400">
                        <div class="relative image overflow-hidden">
                            <img id="preview-image-before-upload" class="w-full mx-auto object-cover" style="height: 223px"
                                src="{{Storage::url($my_profile->avatar)}}"
                                alt="">

                            <div class="absolute bottom-0 right-0 flex justify-center items-center w-12 h-12">
                                <label for="file-upload" class="cursor-pointer">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bg-white p-1 rounded-full" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z" />
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M15 13a3 3 0 11-6 0 3 3 0 016 0z" />
                                    </svg>
                                    <input id="file-upload" name="avatar" type="file" class="sr-only">
                                </label>
                            </div>

                        </div>
                        <h1 class="text-gray-900 font-bold text-xl leading-8 my-3">{{$my_profile->name}}</h1>
                        <!-- <h3 class="text-gray-600 font-lg text-semibold leading-6">Owner at Her Company Inc.</h3> -->

                        <div class="w-full flex mb-2 overflow-x-scroll">
                            @foreach($my_projects as $project)
                                <div>
                                    <img class="rounded-full w-16 h-16" src="{{Storage::url($project->image_url)}}" alt="">
                                </div>
                            @endforeach
                        </div>

                        <ul
                            class="bg-gray-100 text-gray-600 text-sm hover:text-gray-700 hover:shadow py-2 px-3 mt-3 divide-y rounded shadow-sm">
                            <li class="flex items-center py-3">
                                <span>Total Projects</span>
                                <span class="ml-auto">
                                    <span class="bg-blue-500 py-1 px-2 rounded text-white text-sm">{{$my_projects->count()}}</span>
                                </span>
                            </li>
                            <li class="flex items-center py-3">
                                <span>Projects Assigned to</span>
                                <span class="ml-auto">
                                    <span class="bg-blue-500 py-1 px-2 rounded text-white text-sm">{{$assigned_projects->count()}}</span>
                                </span>
                            </li>
                            <li class="flex items-center py-3">
                                <span>Member since</span>
                                <span class="ml-auto">{{ date('M d, Y', strtotime($my_profile->created_at))}}</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-span-6">

                        <div class="bg-white p-3 shadow-sm rounded-sm">
                            <div class="flex items-center space-x-2 font-semibold text-gray-900 leading-8">
                                <span clas="text-green-500">
                                    <svg class="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                            d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                                    </svg>
                                </span>
                                <span class="tracking-wide">About</span>
                            </div>
                            <div class="text-gray-700">
                                <div class="grid md:grid-cols-2 text-sm">
                                    <div class="grid grid-cols-2">
                                        <div class="px-4 py-2 font-semibold">First Name</div>
                                        <div class="px-4 py-2">
                                            <input type="text" name="fname" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-none" placeholder="First Name" value="{{explode(' ', $my_profile->name)[0]}}">
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div class="px-4 py-2 font-semibold">Last Name</div>
                                        <div class="px-4 py-2">
                                            <input type="text" name="lname" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-none" placeholder="Last Name" value="{{explode(' ', $my_profile->name)[1]}}">
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div class="px-4 py-2 font-semibold">Gender</div>
                                        <div class="px-4 py-2">
                                            <select name="gender" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-none">
                                                <option disabled selected>Select Gender</option>
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div class="px-4 py-2 font-semibold">Contact No.</div>
                                        <div class="px-4 py-2">
                                            <input type="tel" name="phone" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-none" placeholder="Phone" value="{{$my_profile->phone}}">
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div class="px-4 py-2 font-semibold">Title</div>
                                        <div class="px-4 py-2">
                                            <input type="text" name="title" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-none" placeholder="Title" value="{{$my_profile->title}}">
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div class="px-4 py-2 font-semibold">Email.</div>
                                        <div class="px-4 py-2">
                                            <input type="email" name="email" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-none" placeholder="Email" value="{{$my_profile->email}}">
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div class="px-4 py-2 font-semibold">Role</div>
                                        <div class="px-8 py-2">{{ucfirst($my_profile->role)}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="my-4"></div>

                        <div class="bg-white p-3 shadow-sm rounded-sm">
                            <div class="flex items-center space-x-2 font-semibold text-gray-900 leading-8">
                                <span clas="text-green-500">

                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z" />
                                    </svg>
                                </span>
                                <span class="tracking-wide">Privacy</span>
                            </div>
                            <div class="text-gray-700">
                                <div class="text-sm">
                                    <div class="grid grid-cols-2">
                                        <div class="px-4 py-2 font-semibold">Current Password</div>
                                        <div class="px-4 py-2">
                                            <input type="password" name="current_pass" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-gray-300" placeholder="Current Password" value="{{$my_profile->password}}">
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div class="px-4 py-2 font-semibold">New Password</div>
                                        <div class="px-4 py-2">
                                            <input type="password" name="new_pass" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-gray-300" placeholder="New Password">
                                        </div>
                                    </div>
                                    <div class="grid grid-cols-2">
                                        <div class="px-4 py-2 font-semibold">Confirm Password</div>
                                        <div class="px-4 py-2">
                                            <input type="password" name="confirm_pass" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-md sm:text-sm border-gray-300" placeholder="Confirm Password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="flex-auto flex space-x-4 mt-8">
                            <button class="py-2 px-6 font-semibold rounded-md bg-blue-500 text-white text-xs" type="submit">
                                Update
                            </button>
                            <button class="py-2 px-6 font-semibold rounded-md border border-gray-200 text-gray-800 text-xs" type="reset">
                                Cancel
                            </button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>

</x-app-layout>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script type="text/javascript">

    $(document).ready(function (e) {
        $('#file-upload').change(function(){
            let reader = new FileReader();

            reader.onload = (e) => {
                $('#preview-image-before-upload').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        });
    });

</script>

@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-lg text-xs text-gray-700']) }}>
    {{ $value ?? $slot }}
</label>

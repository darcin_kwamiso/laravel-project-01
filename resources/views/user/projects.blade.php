<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Projects') }}
        </h2>
    </x-slot>


    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="border-t-4 border-teal-500 rounded-b px-4 py-3 shadow-md bg-red-500 text-white absolute z-50" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Error</p>
                    <p class="text-sm">{{$error}}</p>
                </div>
            </div>
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
        <div class="border-t-4 border-teal-500 rounded-b px-4 py-3 shadow-md bg-green-500 text-white absolute z-50" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Success</p>
                    <p class="text-sm">{{session('success')}}</p>
                </div>
            </div>
        </div>
    @endif

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <section class="grid grid-cols-6 space-x-8">
                <div class="w-full col-span-2">
                    <div class="flex flex-col min-w-0 break-words w-full mb-4 shadow-lg rounded-t bg-blue-500 text-white">
                        <div class="rounded-t mb-0 px-8 py-5 border-0">
                            <h3 class="font-semibold text-base text-white">Projects Assigned To</h3>
                        </div>
                    </div>

                    <div class="divide-y px-8 shadow rounded">
                        @foreach($assigned_projects as $project)

                            <div class="grid grid-cols-5 py-3">
                                <div class="col-span-3">
                                    <h4 class="font-bold text-sm text-gray-600">{{$project->name}}</h4>
                                    <div class="text-xs font-bold text-gray-400">
                                        <span>desc</span>
                                        <span>time</span>
                                    </div>
                                </div>

                                <div class="col-span-2 flex">
                                    <?php
$assigned_users = DB::table('assign_users')
    ->join('users', 'assign_users.user_id', '=', 'users.id')
    ->where('project_id', '=', $project->id)
    ->select('users.*')->get();
?>

                                    @foreach($assigned_users as $user)
                                        <img src="{{Storage::url($user->avatar)}}" alt="..." class="w-6 h-6 rounded-full border-2 border-blueGray-50 object-cover shadow -ml-4">
                                    @endforeach

                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>

                <div class="w-full col-span-4">
                    <div class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded
                    bg-blue-500 text-white">
                        <div class="rounded-t mb-0 px-4 py-3 border-0">
                            <div class="flex flex-wrap items-center">
                                <div class="relative w-full px-4 max-w-full flex-grow flex-1 ">
                                    <div class="flex justify-between items-center">
                                        <h3 class="font-semibold text-base text-white">My Projects</h3>
                                        <div class="flex space-x-3">
                                            <a class="flex text-sm px-3 hover:bg-yellow-500 rounded-md items-center" href="{{route('project.create')}}">
                                                New
                                            </a>
                                            <form class="group relative">
                                                <svg width="20" height="20" fill="currentColor" class="absolute left-3 top-1/2 -mt-2.5 text-gray-400 pointer-events-none group-focus-within:text-blue-500" aria-hidden="true">
                                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" />
                                                </svg>
                                                <input class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300 text-gray-500 pl-10" type="text" aria-label="Filter projects" placeholder="Filter projects...">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block w-full overflow-x-auto ">
                            <table class="items-center w-full bg-transparent border-collapse">
                                <thead>
                                    <tr>
                                        <th class="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blue-800 text-blue-300 border-blue-700">Project</th>
                                        <th class="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blue-800 text-blue-300 border-blue-700">Status</th>
                                        <th class="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blue-800 text-blue-300 border-blue-700">Members</th>
                                        <th class="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blue-800 text-blue-300 border-blue-700">Date</th>
                                        <th class="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blue-800 text-blue-300 border-blue-700">Action</th>
                                    </tr>
                                </thead>

                                <tbody class="divide-y">
                                    @foreach($my_projects as $project)
                                        <tr>
                                            <th class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-sm whitespace-nowrap p-4 text-left flex items-center">
                                                @if($project->image_url == 'N/A')
                                                    <small class="h-12 w-12 font-thin text-xs text-blue-500 bg-white rounded-full border flex justify-center items-center ring-2 ring-white">
                                                        no image
                                                    </small>
                                                @else
                                                    <img src="{{Storage::url($project->image_url)}}" class="h-12 w-12 bg-white rounded-full border object-cover ring-1 ring-white" alt="...">
                                                @endif

                                                <div class="flex flex-col">
                                                    <span class="ml-3 font-bold text-white"> {{ucwords($project->name)}} </span>
                                                    <small class="ml-3 text-gray-300"> {{ucwords($project->name)}} </small>
                                                </div>
                                            </th>
                                            <td class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                <i class="fas fa-circle text-yellow-500 mr-2"></i>{{$project->status}}</td>
                                            <td class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                <div class="flex">

                                                    <?php
$assigned_users = DB::table('assign_users')
    ->join('users', 'assign_users.user_id', '=', 'users.id')
    ->where('project_id', '=', $project->id)
    ->select('users.*')->get();
?>

                                                    @foreach($assigned_users as $user)
                                                        <img src="{{Storage::url($user->avatar)}}" alt="..." class="w-6 h-6 rounded-full border-2 border-blueGray-50 object-cover shadow -ml-4">
                                                    @endforeach

                                                </div>
                                            </td>
                                            <td class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                <span>{{date('M d, Y', strtotime($project->created_at))}}</span>
                                                <!-- <small>{{date('g:i A', strtotime($project->created_at))}}</small> -->
                                            </td>

                                            <td class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4">
                                                <div class="flex items-center space-x-3">
                                                    <a href="{{route('project.show', $project->id)}}" class="text-white hover:text-yellow-500">Preview</a>
                                                    <a href="{{route('project.edit', $project->id)}}" class="text-white hover:text-yellow-500">Edit</a>
                                                    <form action="{{route('project.destroy', $project->id)}}" method="POST">
                                                        @csrf
                                                        @method('delete')
                                                        <input onclick="return confirm('are you sure want to delete?')" class="cursor-pointer text-white hover:text-red-500" type="submit" value="Delete">
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{$my_projects->links()}}
                </div>

            </section>

        </div>
    </div>
</x-app-layout>

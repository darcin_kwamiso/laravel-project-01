<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Project') }}
        </h2>
    </x-slot>


    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="border-t-4 border-teal-500 rounded-b px-4 py-3 shadow-md bg-red-500 text-white absolute z-50" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Error</p>
                    <p class="text-sm">{{$error}}</p>
                </div>
            </div>
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
        <div class="border-t-4 border-teal-500 rounded-b px-4 py-3 shadow-md bg-green-500 text-white absolute z-50" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Success</p>
                    <p class="text-sm">{{session('success')}}</p>
                </div>
            </div>
        </div>
    @endif

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="p-6 grid grid-cols-8 space-x-4">
            <div class="col-span-5">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                    <div class="pr-4 pt-8 pb-3 bg-white border-b border-gray-200">
                        @if(request()->routeIs('project.create'))

                            <form class="grid grid-cols-2 space-x-4" action="{{route('project.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input hidden type="text" name="user_id" value="{{Auth::user()->id}}">
                                <div class="col-span-1">
                                    <div class="col-span-3 mb-4 w-full space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Name</label>
                                        <input class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300" type="text" placeholder="Name..." name="name" />
                                    </div>

                                    <div class="mb-4 space-y-2">
                                        <label for="company-website" class="block text-sm font-medium text-gray-700">
                                            Git Link
                                        </label>
                                        <div class="mt-1 flex rounded-md shadow-sm">
                                            <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                                https://
                                            </span>
                                            <input type="text" name="link" id="company-website" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300" placeholder="www.example.com">
                                        </div>
                                    </div>

                                    <div class="col-span-2 mb-4 space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Category</label>
                                        <div class="space-x-4">
                                            <select name="category" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300">
                                                <option selected disabled>Category...</option>
                                                @foreach($all_categories as $category)
                                                    <option value="{{$category->id}}">{{$category->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-span-2 mb-4 space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Status</label>
                                        <div class="space-x-4">
                                            <select name="status" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300">
                                                <option selected disabled>Status...</option>
                                                <option value="Active">Active</option>
                                                <option value="Stuck">Stuck</option>
                                                <option value="Ongoing">Ongoing</option>
                                                <option value="Incomplete">Incomplete</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="mb-4 space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Description</label>
                                        <textarea class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300 resize-none" id="summernote" name="description" cols="30" rows="4" placeholder="Description..."></textarea>
                                    </div>

                                    <div class="flex-auto flex space-x-4">
                                        <button class="py-2 px-6 font-semibold rounded-md bg-blue-500 text-white text-xs" type="submit">
                                            Create
                                        </button>
                                        <button class="py-2 px-6 font-semibold rounded-md border border-gray-200 text-gray-800 text-xs" type="reset">
                                            Cancel
                                        </button>
                                    </div>

                                </div>

                                <div class="col-span-1">
                                    <div class="mb-4">
                                        <label class="block text-sm font-medium text-gray-700">Image</label>
                                        <div class="mt-1 flex justify-center px-6 pt-3 pb-4 border-2 border-gray-300 border-dashed rounded-md">
                                            <div class="space-y-1 text-center">
                                                <svg class="mx-auto h-10 w-10 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                                                    <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                </svg>
                                                <div class="flex text-sm text-gray-600">
                                                    <label for="image_upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                        <span>upload</span>
                                                        <input id="image_upload" name="image_upload" type="file" class="sr-only" accept="image/jpeg, image/png, image/gif">
                                                    </label>
                                                    <p class="pl-1">or drag and drop</p>
                                                </div>
                                                <p class="text-xs text-gray-500">
                                                    PNG, JPG, GIF up to 10MB
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <label class="block text-sm font-medium text-gray-700">Video</label>
                                        <div class="mt-1 flex justify-center px-6 pt-3 pb-4 border-2 border-gray-300 border-dashed rounded-md">
                                            <div class="space-y-1 text-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="mx-auto h-10 w-10 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M17 14v6m-3-3h6M6 10h2a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2zm10 0h2a2 2 0 002-2V6a2 2 0 00-2-2h-2a2 2 0 00-2 2v2a2 2 0 002 2zM6 20h2a2 2 0 002-2v-2a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2z" />
                                                </svg>
                                                <div class="flex text-sm text-gray-600">
                                                    <label for="video_upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                    <span>upload</span>
                                                    <input id="video_upload" name="video_upload" type="file" class="sr-only" accept="video/*">
                                                    </label>
                                                    <p class="pl-1">or drag and drop</p>
                                                </div>
                                                <p class="text-xs text-gray-500">
                                                    MPG, MP4, AVI up to 128MB
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <label class="block text-sm font-medium text-gray-700">Document</label>
                                        <div class="mt-1 flex justify-center px-6 pt-3 pb-4 border-2 border-gray-300 border-dashed rounded-md">
                                            <div class="space-y-1 text-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="mx-auto h-10 w-10 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                                                </svg>
                                                <div class="flex text-sm text-gray-600">
                                                    <label for="document_upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                    <span>upload</span>
                                                    <input id="document_upload" name="document_upload" type="file" class="sr-only" accept="application/pdf, application/vnd.ms-excel">
                                                    </label>
                                                    <p class="pl-1">or drag and drop</p>
                                                </div>
                                                <p class="text-xs text-gray-500">
                                                    TXT, DOC, XLS up to 10MB
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>

                        @else
                            <form class="grid grid-cols-2 space-x-4" action="{{route('project.update', $edit_project->id)}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <input hidden type="text" name="user_id" value="{{Auth::user()->id}}">
                                <div class="col-span-1">
                                    <div class="col-span-3 mb-4 w-full space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Name</label>
                                        <input class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300" type="text" placeholder="Name..." name="name" value="{{$edit_project->name}}" />
                                    </div>

                                    <div class="mb-4 space-y-2">
                                        <label for="company-website" class="block text-sm font-medium text-gray-700">
                                            Git Link
                                        </label>
                                        <div class="mt-1 flex rounded-md shadow-sm">
                                            <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                                https://
                                            </span>
                                            <input type="text" name="link" id="company-website" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300" placeholder="www.example.com" value="{{$edit_project->link}}">
                                        </div>
                                    </div>

                                    <div class="col-span-2 mb-4 space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Category</label>
                                        <div class="space-x-4">
                                            <select name="category" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300">
                                                <option selected disabled>Category...</option>
                                                @foreach($all_categories as $category)
                                                    <option value="{{$category->id}}">{{$category->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-span-2 mb-4 space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Status</label>
                                        <div class="space-x-4">
                                            <select name="status" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300">
                                                <option selected disabled>Status...</option>
                                                <option value="Active">Active</option>
                                                <option value="Stuck">Stuck</option>
                                                <option value="Ongoing">Ongoing</option>
                                                <option value="Incomplete">Incomplete</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="mb-4 space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Description</label>
                                        <textarea class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300 resize-none" id="summernote" name="description" cols="30" rows="4" placeholder="Description...">{{$edit_project->description}}
                                        </textarea>
                                    </div>

                                    <div class="flex-auto flex space-x-4">
                                        <button class="py-2 px-6 font-semibold rounded-md bg-blue-500 text-white text-xs" type="submit">
                                            Update
                                        </button>
                                        <button class="py-2 px-6 font-semibold rounded-md border border-gray-200 text-gray-800 text-xs" type="reset">
                                            Cancel
                                        </button>
                                    </div>
                                </div>

                                <div class="col-span-1">
                                    <div class="mb-4">
                                        <label class="block text-sm font-medium text-gray-700">Image</label>
                                        <div class="mt-1 flex justify-center px-6 pt-3 pb-4 border-2 border-gray-300 border-dashed rounded-md">
                                            <div class="space-y-1 text-center">
                                                <svg class="mx-auto h-10 w-10 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                                                    <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                                </svg>

                                                <!-- <img id="preview-image-before-upload" class="mx-auto h-8 w-8 rounded text-gray-400" src="" alt=""> -->

                                                <div class="flex text-sm text-gray-600">
                                                    <label for="image-upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                    <span>upload</span>
                                                    <input id="image-upload" name="image_upload" type="file" class="sr-only" accept="image/jpeg, image/png, image/gif">
                                                    </label>
                                                    <p class="pl-1">or drag and drop</p>
                                                </div>
                                                <p class="text-xs text-gray-500">
                                                    PNG, JPG, GIF up to 10MB
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <label class="block text-sm font-medium text-gray-700">Video</label>
                                        <div class="mt-1 flex justify-center px-6 pt-3 pb-4 border-2 border-gray-300 border-dashed rounded-md">
                                            <div class="space-y-1 text-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="mx-auto h-10 w-10 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M17 14v6m-3-3h6M6 10h2a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2zm10 0h2a2 2 0 002-2V6a2 2 0 00-2-2h-2a2 2 0 00-2 2v2a2 2 0 002 2zM6 20h2a2 2 0 002-2v-2a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2z" />
                                                </svg>
                                                <div class="flex text-sm text-gray-600">
                                                    <label for="video_upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                    <span>upload</span>
                                                    <input id="video_upload" name="video_upload" type="file" class="sr-only" accept="video/*">
                                                    </label>
                                                    <p class="pl-1">or drag and drop</p>
                                                </div>
                                                <p class="text-xs text-gray-500">
                                                    MPG, MP4, AVI up to 128MB
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <label class="block text-sm font-medium text-gray-700">Document</label>
                                        <div class="mt-1 flex justify-center px-6 pt-3 pb-4 border-2 border-gray-300 border-dashed rounded-md">
                                            <div class="space-y-1 text-center">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="mx-auto h-10 w-10 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M9 13h6m-3-3v6m5 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
                                                </svg>
                                                <div class="flex text-sm text-gray-600">
                                                    <label for="document_upload" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                    <span>upload</span>
                                                    <input id="document_upload" name="document_upload" type="file" class="sr-only" accept="application/pdf, application/vnd.ms-excel">
                                                    </label>
                                                    <p class="pl-1">or drag and drop</p>
                                                </div>
                                                <p class="text-xs text-gray-500">
                                                    TXT, DOC, XLS up to 10MB
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        @endif
                    </div>

                </div>
            </div>

            <div class="col-span-3">
                <div class="rounded-t mb-0 px-4 py-4 border-0 bg-blue-500">
                    <div class="flex flex-wrap items-center">
                        <div class="relative w-full px-4 max-w-full flex-grow flex-1 ">
                            <div class="flex justify-between">
                                <h3 class="font-semibold text-sm text-white">My Projects</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="xl:pb-3 text-sm leading-6">
                    @foreach($my_projects as $project)
                        <li class="flex mt-2">
                            <a href="{{route('project.show', $project->id)}}" class="w-full hover:bg-blue-500 hover:ring-blue-500 hover:shadow-md group rounded-md px-8 py-3 bg-white ring-1 ring-gray-200 shadow-sm">
                                <div class="grid grid-cols-6 space-x-4">
                                    <div class="col-span-4">
                                        <dl class="grid sm:block lg:grid xl:block grid-cols-2 grid-rows-2 items-center">
                                            <div>
                                                <dt class="sr-only">Title</dt>
                                                <dd class="group-hover:text-white font-bold text-gray-900 text-base">
                                                    {{$project->name}}
                                                </dd>
                                            </div>
                                            <div>
                                                <dt class="sr-only">Category</dt>
                                                <dd class="group-hover:text-blue-200">{{$project->title}}</dd>
                                            </div>
                                            <div class="col-start-2 row-start-1 row-end-3 mt-2">
                                                <div class="flex -space-x-1 h-6">
<?php
$assigned_users = DB::table('assign_users')
    ->join('users', 'assign_users.user_id', '=', 'users.id')
    ->where('project_id', '=', $project->id)
    ->select('users.*')->get();
?>
                                                    @foreach($assigned_users as $user)
                                                        <img class="inline-block h-6 w-6 rounded-full ring-2 ring-white object-cover" src="{{Storage::url($user->avatar)}}" alt="">
                                                    @endforeach

                                                </div>
                                            </div>
                                        </dl>
                                    </div>

                                    <div class="col-span-2">
                                        <div class="flex justify-center items-center">
                                            @if($project->image_url == 'N/A')
                                                <small class="h-16 w-24 font-thin text-xs text-blue-500 bg-white rounded border flex justify-center items-center">
                                                    no image
                                                </small>
                                            @else
                                                <img class="h-16 w-24 rounded object-cover" src="{{Storage::url($project->image_url)}}" alt="">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>

                {{$my_projects->links()}}
            </div>
        </div>
    </div>

</x-app-layout>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script type="text/javascript">

    $(document).ready(function (e) {
        $('#image-upload').change(function(){
            let reader = new FileReader();

            reader.onload = (e) => {
                $('#preview-image-before-upload').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        });
    });

</script>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-6">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-scroll">

                    <div class="grid grid-cols-2 space-x-8">
                        <div class="col-span-1 bg-white border-b border-gray-200 shadow-sm rounded-lg">
                            <div class="flex items-center justify-between bg-blue-500 px-6 py-5">
                                <h3 class="font-semibold text-white">Charts</h3>
                            </div>

                            @include('charts.google-bar-chart')
                        </div>

                        <div class="col-span-1 bg-white border-b border-gray-200 shadow-sm rounded-lg">

                            <section>
                                <header class="bg-white space-y-4 p-4 sm:px-8 sm:py-6 lg:p-4 xl:px-8 xl:py-6">
                                    <div class="flex items-center justify-between">
                                        <h2 class="font-semibold text-gray-900">My Projects</h2>
                                        <a href="{{route('project.index')}}" class="hover:bg-blue-400 group flex items-center rounded-md bg-blue-500 text-white text-xs font-medium px-2 py-3 shadow-sm">
                                            All My Projects
                                        </a>
                                    </div>
                                    <form class="group relative">
                                        <svg width="20" height="20" fill="currentColor" class="absolute left-3 top-1/2 -mt-2.5 text-gray-400 pointer-events-none group-focus-within:text-blue-500" aria-hidden="true">
                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" />
                                        </svg>
                                        <input class="focus:ring-1 focus:ring-blue-500 focus:outline-none w-full text-sm leading-6 text-gray-900 placeholder-gray-400 rounded-md py-2 pl-10 ring-1 ring-gray-200 shadow-sm" type="text" aria-label="Filter projects" placeholder="Filter projects...">
                                    </form>
                                </header>
                                <ul class="p-4 sm:px-8 sm:pt-2 sm:pb-8 lg:p-4 xl:px-8 xl:pt-2 xl:pb-8 grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-1 xl:grid-cols-2 gap-4 text-sm leading-6">
                                    <li class="flex">
                                        <a href="{{route('project.create')}}" class="hover:border-blue-500 hover:border-solid hover:bg-white hover:text-blue-500 group w-full flex flex-col items-center justify-center rounded-md border-2 border-dashed border-gray-300 text-sm leading-6 text-gray-900 font-medium py-3 h-28">
                                            <svg class="group-hover:text-blue-500 mb-1 text-gray-400" width="20" height="20" fill="currentColor" aria-hidden="true">
                                            <path d="M10 5a1 1 0 0 1 1 1v3h3a1 1 0 1 1 0 2h-3v3a1 1 0 1 1-2 0v-3H6a1 1 0 1 1 0-2h3V6a1 1 0 0 1 1-1Z" />
                                            </svg>
                                            New project
                                        </a>
                                    </li>
                                    @foreach($my_projects as $project)
                                        <li>
                                            <a href="{{route('project.show', $project->id)}}" class="hover:bg-blue-500 hover:ring-blue-500 hover:shadow-md group rounded-md p-3 bg-white ring-1 ring-gray-200 shadow-sm w-full flex flex-col">
                                                <dl class="grid sm:block lg:grid xl:block grid-cols-2 grid-rows-2 items-center">
                                                    <div>
                                                        <dt class="sr-only">Title</dt>
                                                        <dd class="group-hover:text-white font-semibold text-gray-900">
                                                            {{ucfirst($project->name)}}
                                                        </dd>
                                                    </div>
                                                    <div>
                                                        <dt class="sr-only">Category</dt>
                                                        <dd class="group-hover:text-blue-200">{{ucfirst($project->title)}}</dd>
                                                    </div>
                                                    <div class="col-start-2 row-start-1 row-end-3 sm:mt-4 lg:mt-0 xl:mt-4">
                                                        <dt class="sr-only">Users</dt>
                                                        <dd x-for="user in project.users" class="flex justify-end sm:justify-start lg:justify-end xl:justify-start -space-x-1.5">
                                                            <div class="flex -space-x-1 h-6">
        <?php
$assigned_users = DB::table('assign_users')
    ->join('users', 'assign_users.user_id', '=', 'users.id')
    ->where('project_id', '=', $project->id)
    ->select('users.*')->get();
?>

                                                                @foreach($assigned_users as $user)
                                                                    <img class="inline-block h-6 w-6 rounded-full ring-2 ring-white object-cover" src="{{Storage::url($user->avatar)}}" alt="">
                                                                @endforeach

                                                            </div>
                                                        </dd>
                                                    </div>
                                                </dl>
                                            </a>
                                        </li>
                                    @endforeach

                                </ul>
                            </section>

                        </div>
                    </div>
            </div>
        </div>
    </div>
</x-app-layout>

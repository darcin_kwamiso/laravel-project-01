<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Settings') }}
        </h2>
    </x-slot>

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="border-t-4 border-teal-500 rounded-b px-4 py-3 shadow-md bg-red-500 text-white absolute z-50" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Error</p>
                    <p class="text-sm">{{$error}}</p>
                </div>
            </div>
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
        <div class="border-t-4 border-teal-500 rounded-b px-4 py-3 shadow-md bg-green-500 text-white absolute z-50" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Success</p>
                    <p class="text-sm">{{session('success')}}</p>
                </div>
            </div>
        </div>
    @endif

    <div class="max-w-7xl mx-auto mt-5">
        <div class="grid grid-cols-8 space-x-8" style="height: 560px;">
            <div class="col-span-3 bg-white p-4 rounded relative">
                <div class="text-center">
                    <div class="flex justify-center items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-12 w-12 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                        </svg>
                    </div>

                    <h4 class="mx-auto font-bold text-gray-600 drop-shadow-sm">Add Team Members</h4>
                    <p class="mx-auto text-gray-500 text-xs drop-shadow-sm">You can assign up to 5 contributors as viewers and 2 as authors.
                        As the owner of the proeject, can manage team member permissions
                    </p>
                </div>

                <div class="">
                    <div class="my-2">
                        <form class="group relative">
                            <svg width="20" height="20" fill="currentColor" class="absolute left-3 top-1/2 -mt-2.5 text-gray-400 pointer-events-none group-focus-within:text-blue-500" aria-hidden="true">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" />
                            </svg>
                            <input class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300 pl-10" type="text" aria-label="Filter projects" placeholder="search...">
                        </form>
                    </div>
                    <div>
                        <ul class="divide-y">

                            @foreach($all_users as $user)
                                <li class="grid grid-cols-9 items-center gap-4 py-3 px-4">
                                    <div class="col-span-1">
                                        <img class="w-full h-8 rounded-full object-cover ring-2 ring-white" src="{{Storage::url($user->avatar)}}" alt="">
                                    </div>
                                    <div class="col-span-6">
                                        <h3 class="font-bold text-base text-gray-700">{{$user->name}}</h3>
                                        <p class="font-bold text-gray-400 text-xs">{{$user->title}}</p>
                                    </div>
                                    <form class="col-span-1 flex items-center space-x-3" action="{{route('assign.store')}}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div x-data="{ dropdownOpen: false }" class="relative">
                                            <button type="button" @click="dropdownOpen = !dropdownOpen" class="relative z-10 block bg-transparent rounded p-1 focus:outline-none">
                                                <svg class="h-6 w-6 text-gray-400" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z" />
                                                </svg>
                                            </button>

                                            <input class="hidden" type="text" name="user_id" value="{{$user->id}}">
                                            <input class="hidden" type="text" name="project_id" value="{{$current_project->id}}">

                                            <div x-show="dropdownOpen" @click="dropdownOpen = false" class="fixed inset-0 h-full w-full z-10"></div>

                                            <div x-show="dropdownOpen" class="divide-y absolute right-0 mt-2 w-60 bg-gray-100 rounded-md overflow-hidden shadow-xl z-20">

                                                <div class="w-full px-2 py-3 text-xs">
                                                    <label class="text-gray-700 font-semibold">Assign as</label>
                                                    <select class="my-2 w-full border-none text-sm rounded" name="roles" id="">
                                                        <option class="p-3" selected disabled>Select Role</option>
                                                        <option class="p-3" value="author">Author</option>
                                                        <option class="p-3" value="viewer">Viewer</option>
                                                    </select>
                                                    <input class="w-full bg-blue-500 p-2 rounded text-white cursor-pointer hover:bg-yellow-500" type="submit" value="submit">
                                                </div>

                                            </div>
                                        </div>
                                    </form>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>

            </div>

            <div class="col-span-5 bg-white rounded overflow-hidden">
                <div class="flex font-sans">

                    <div class="flex-auto p-6">
                        <div class="flex flex-wrap">
                            <h1 class="flex-auto text-lg font-bold text-gray-600 drop-shadow-sm">
                                {{$current_project->name}}
                            </h1>
                            <div class="text-sm font-semibold text-gray-400 text-right">
                                Members: {{$assigned_users->count()}}

                                <div class="space-x-2 flex text-sm">
                                    {{$current_project->title}}
                                </div>
                            </div>
                            <div class="w-full flex-none font-medium mt-1 text-gray-500 text-xs drop-shadow-sm">
                                {{$current_project->description}}
                            </div>
                        </div>
                        <div class="flex items-baseline mb-3 pb-4 border-b border-gray-200 text-gray-500 text-xs drop-shadow-sm">
                            <div class="space-x-1 flex text-sm">
                                <span>git link:</span>
                                <a href="http://{{$current_project->link}}" target="_blank" rel="noopener noreferrer">{{$current_project->link}}</a>
                            </div>
                        </div>
                        <div class="text-sm font-medium">
                            <h3 class="mx-auto text-center pb-3 text-gray-500 text-sm">Media and Docs</h3>
                            <div class="flex justify-between space-x-4">
                                <div class="h-16 w-48 px-6 font-semibold rounded-md border border-gray-150 text-gray-400 flex justify-center items-center">
                                    @if($current_project->image_url == 'N/A')
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                                        </svg>
                                    @else
                                        <img class="w-full h-full object-cover" src="{{Storage::url($current_project->image_url)}}" alt="">
                                    @endif
                                </div>
                                <div class="h-16 w-48 px-6 font-semibold rounded-md border border-gray-150 text-gray-400 flex justify-center items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z" />
                                    </svg>
                                </div>
                                <div class="h-16 w-48 px-6 font-semibold rounded-md border border-gray-150 text-gray-400 flex justify-center items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M7 4v16M17 4v16M3 8h4m10 0h4M3 12h18M3 16h4m10 0h4M4 20h16a1 1 0 001-1V5a1 1 0 00-1-1H4a1 1 0 00-1 1v14a1 1 0 001 1z" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- component -->
                <section class="max-w-6xl mx-auto px-4 sm:px-6 lg:px-4">
                    <div class="text-center pb-2">
                        <h2 class="text-sm text-blue-500">
                            Members Assigned to this Project
                        </h2>
                    </div>
                    <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-5 gap-3">
                        @foreach($assigned_users as $user)
                            <div class="w-full bg-blue-500 rounded-lg sahdow-lg flex flex-col justify-center items-center py-4">
                                <div class="mb-2">
                                    @if($user->avatar == 'N/A')
                                        <small class="h-20 w-20 font-thin text-xs text-blue-500 bg-white rounded-full border flex justify-center items-center">
                                            no image
                                        </small>
                                    @else
                                        <img class="object-center object-cover rounded-full h-20 w-20 ring-2 ring-white" src="{{Storage::url($user->avatar)}}" alt="">
                                    @endif
                                </div>
                                <div class="text-center">
                                    <p class="text-sm text-white font-bold">{{$user->name}}</p>
                                    <p class="text-xs text-gray-200 font-normal">{{ucfirst($user->title)}}</p>

                                    <form action="{{route('assign.destroy', $user->id)}}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <input type="submit" value="remove" class="inline-flex items-center justify-center px-3 py-2 mt-1 text-xs font-thin leading-none text-red-100 bg-red-600 rounded-full hover:bg-red-500 cursor-pointer">
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </section>

            </div>
        </div>
    </div>

</x-app-layout>

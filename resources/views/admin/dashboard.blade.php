<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-6">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <div class="flex justify-between mb-4">
                <div class="h-20 w-72 px-6 py-4 rounded-md bg-white border">
                    <div class="grid grid-cols-3">
                        <div class="col-span-2">
                            <h1>{{$new_users->count()}}</h1>
                            <p>Today Users</p>
                        </div>
                        <div class="col-span-1 flex justify-center items-center rounded-full border">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                            </svg>
                        </div>
                    </div>
                </div>

                <div class="h-20 w-72 px-6 py-4 rounded-md bg-white border">
                    <div class="grid grid-cols-3">
                        <div class="col-span-2">
                            <h1>{{$users->count()}}</h1>
                            <p>All Users</p>
                        </div>
                        <div class="col-span-1 flex justify-center items-center rounded-full border">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                            </svg>
                        </div>
                    </div>
                </div>

                <div class="h-20 w-72 px-6 py-4 rounded-md bg-white border">
                    <div class="grid grid-cols-3">
                        <div class="col-span-2">
                            <h1>{{$new_projects->count()}}</h1>
                            <p>Today Projects</p>
                        </div>
                        <div class="col-span-1 flex justify-center items-center rounded-full border">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M8 13v-1m4 1v-3m4 3V8M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z" />
                            </svg>
                        </div>
                    </div>
                </div>

                <div class="h-20 w-72 px-6 py-4 rounded-md bg-white border">
                    <div class="grid grid-cols-3">
                        <div class="col-span-2">
                            <h1>{{$projects->count()}}</h1>
                            <p>All Projects</p>
                        </div>
                        <div class="col-span-1 flex justify-center items-center rounded-full border">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M7 12l3-3 3 3 4-4M8 21l4-4 4 4M3 4h18M4 4h16v12a1 1 0 01-1 1H5a1 1 0 01-1-1V4z" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="grid grid-cols-5">
                        <div class="col-span-3">
                            @include('charts.google-bar-chart')
                        </div>

                        <div class="col-span-2">
                            @include('charts.google-pie-chart')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Settings') }}
        </h2>
    </x-slot>

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="border-t-4 border-teal-500 rounded-b px-4 py-3 shadow-md bg-red-500 text-white absolute z-50" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Error</p>
                    <p class="text-sm">{{$error}}</p>
                </div>
            </div>
        </div>
        @endforeach
    @endif

    @if(Session::has('success'))
        <div class="border-t-4 border-teal-500 rounded-b px-4 py-3 shadow-md bg-green-500 text-white absolute z-50" role="alert">
            <div class="flex items-center">
                <div class="py-1">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                </div>
                <div>
                    <p class="font-bold">Success</p>
                    <p class="text-sm">{{session('success')}}</p>
                </div>
            </div>
        </div>
    @endif

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mt-5">
        <div class="md:grid md:grid-cols-3 gap-4">
            <div class="md:col-span-1">
                <div class="bg-blue-500 overflow-hidden shadow-md sm:rounded-md">
                    <div class="pt-8 pb-4 bg-blue-500 border-b border-gray-200">
                        <div class="px-4 sm:px-0">
                            <h3 class="block mx-4 font-semibold text-lg text-white">All Categories</h3>
                            <div class="mt-2 flex rounded-md shadow-sm mx-4">
                                <input type="text" name="search" class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-l-md sm:text-sm border-gray-300" placeholder="search...">
                                <button type="submit" class="inline-flex items-center px-3 rounded-r-md border border-l-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                    search
                                </button>
                            </div>
                            <div class="mt-2 text-sm text-gray-600" style="height: 420px; overflow-y: schroll;">

                                <ul class="divide-y divide-gray-100">
                                    @foreach($all_categories as $category)
                                        <li class="px-4 py-3 grid grid-cols-8 space-x-4 bg-blue-500 hover:bg-blue-500 cursor-pointer mx-4">
                                            <div class="col-span-6">
                                                <a href="{{route('categories.edit', $category->id)}}" class="font-bold text-white overflow-ellipsis capitalize text-sm">{{$category->title}}</a>
                                                <p class="overflow-ellipsis w-full font-thin capitalize text-gray-200 text-xs">{{$category->desc}}</p>
                                            </div>
                                            <div class="inline-flex items-center space-x-3 text-white col-span-1">
                                                <a href="{{route('categories.edit', $category->id)}}" class="text-gray-300 hover:text-gray-300 font-bold py-2 px-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                                    </svg>
                                                </a>
                                                <form method="POST" action="{{route('categories.destroy', $category->id)}}">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" onclick="return confirm('are you sure want to delete?')" class="text-red-500 hover:text-red-600 font-bold py-2">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M6 18L18 6M6 6l12 12" />
                                                        </svg>
                                                    </button>
                                                </form>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="md:col-span-2">
                <div class="max-w-7xl mx-auto">
                    <div class="bg-white overflow-hidden shadow-sm sm:rounded-md">
                        <div class="px-4 pt-8 pb-3 bg-white border-b border-gray-200" style="height: 550px;">
                            @if(request()->routeIs('categories.index'))
                                <form action="{{route('categories.store')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('post')
                                    <div class="mb-4 w-full space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Title</label>
                                        <input class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300" type="text" placeholder="Title..." name="title" />
                                    </div>

                                    <div class="mb-4 space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Description</label>
                                        <textarea class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300 resize-none" name="desc" cols="30" rows="4" placeholder="Description..."></textarea>
                                    </div>

                                    <div class="flex space-x-3 mb-4 text-sm font-medium">
                                        <div class="flex-auto flex space-x-3">
                                            <button class="w-1/8 py-2 px-4 flex items-center justify-center rounded-md bg-blue-500 text-white" type="submit">Save</button>
                                            <a href="{{route('categories.index')}}" class="w-1/8 py-2 px-4 flex items-center justify-center rounded-md border border-gray-300" type="reset">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            @else
                                <form action="{{route('categories.update', $edit_category->id)}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="mb-4 w-full space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Title</label>
                                        <input class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300" type="text" placeholder="Title..." name="title" value="{{$edit_category->title}}" />
                                    </div>

                                    <div class="mb-4 space-y-2">
                                        <label class="block text-sm font-medium text-gray-700">Description</label>
                                        <textarea class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300 resize-none" name="desc" cols="30" rows="4" placeholder="Description...">{{$edit_category->desc}}</textarea>
                                    </div>

                                    <div class="flex space-x-3 mb-4 text-sm font-medium">
                                        <div class="flex-auto flex space-x-3">
                                            <button class="w-1/8 py-2 px-4 flex items-center justify-center rounded-md bg-blue-500 text-white" type="submit">Update</button>
                                            <a href="{{route('categories.index')}}" class="w-1/8 py-2 px-4 flex items-center justify-center rounded-md border border-gray-300" type="reset">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
